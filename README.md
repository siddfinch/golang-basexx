# golang-baseXX

A baseXX encoder/decoder. Started out as a base62 decoder/encoder originally part of the myturl.com's TCL code. Improved when ported to Go and make it possible to use other bases. I'm just that type of guy. 