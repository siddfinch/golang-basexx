package basexx

import (
	"bytes"
	"testing"
)

// BASE62 should always be 0-9,a-z,A-Z
const alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

var cases = map[uint][]byte{

	0:                    []byte("0"),
	1:                    []byte("1"),
	100:                  []byte("1C"),
	999:                  []byte("g7"),
	9999999999:           []byte("aUKYOz"),
	13518126808285626735: []byte("g6Babbcj0sv"),
}

func TestEncode(t *testing.T) {
	for input, expected := range cases {
		output := Encode(alphabet, input)
		if !bytes.Equal(expected, output) {
			t.Errorf("Encode(%d), Got %d, Expected: %d", input, output, expected)
		}
	}
}

func TestDecode(t *testing.T) {
	for expected, input := range cases {
		output := Decode(alphabet, input)
		if expected != output {
			t.Errorf("Decode(%s) => Got: %d,  Expected: %d", input, output, expected)
		}
	}
}
