package basexx

import (
	"bytes"
	"math"
)

// Encode from alphabet string a, the number n)
func Encode(a string, n uint) []byte {

	//Get the base value
	var base = uint(len(a))

	if n == 0 {
		return []byte{a[0]}
	}

	// the result is a byte array
	var result []byte

	// We'll divide the number byte the base. What remains is what
	// we'll use the calculate the number. But, this will give us the
	// number in reverse order. So, after we are done, we'll need to
	// reverse the byte array. This concludes the math portion
	// of the comments.
	var remain uint

	for n != 0 {
		// get the remainder .. this is used to get the string
		// from the alphabet string
		remain = n % base

		// Change n
		n = n / base

		result = append(result, a[remain])

	}

	// Like I said, reversal time.
	// x= start of the result
	// y = end of the result
	// swap postions and move closer to the center
	// stop when x is bigger than y, you have crossed the center
	for x, y := 0, len(result)-1; x < y; x, y = x+1, y-1 {
		result[x], result[y] = result[y], result[x]
	}

	return result
}

// Decode .. given a string and an encoded byte array, return the number
func Decode(a string, value []byte) uint {

	//Get the base value
	var base = uint(len(a))

	// we'll need to convert our string to a byte array
	var ba = []byte(a)

	// Sorry, I lied .. more math
	// To decode the byte array value, we have to walk
	// through the value, starting at the last
	length := len(value)
	n := uint(0)
	i := 0

	for _, c := range value {
		// Find the location of the charactor in the byte array
		loc := uint(bytes.IndexByte(ba, c))
		// This value will need to be raised to a specific power
		// 43210. So a value like abcde would
		// be some like
		// c=a, loc = 10, exp = 4, val = 10 * base^exp .. i.e.: 10 * 62^4
		// c=b, loc = 11, exp = 3, val = val + 11 * 62^3 .. etc
		// c=c, loc = 12, exp = 2
		// c=d, loc = 13, exp = 1
		// c=e, loc = 14, exp = 0
		exp := (length - (i + 1))
		n = n + loc*uint(math.Pow(float64(base), float64(exp)))
		i++
	}
	return n
}
