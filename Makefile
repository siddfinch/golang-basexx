test: basexx.go basexx_test.go
	@go test

lint: basexx.go basexx_test.go
	@golint -set_exit_status `go list ./... | grep -v /vendor/`
